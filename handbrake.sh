#! /bin/bash --

#
# Manage a unit's 'handbrake' state'
#
# Usage:
#
#   handbrake
#   or                      - print 'on' or 'off' as appropriate
#   handbrake status          . return success if status is 'on'
# 
#   handbrake off           - turn off handbrake status
#                             . print nothing if this works, or error message if not
#                             . return success if this works
#                             . will fail if not already in handbrake mode
#
#   handbrake on           - turn on handbrake status
#                             . print nothing if this works, or error message if not
#                             . return success if this works
#                             . will fail if already in handbrake mode

progname="$(basename $0)"

# Temporary files.
# Create in this directory in main program.
tempdir="$(mktemp -d -p "${TMPDIR-/tmp}" "$progname.XXXXXXXXXX")"
trap 'rm -r "$tempdir"' EXIT
trap 'rm -r "$tempdir"; trap - EXIT; exit 99' 1 2 3 15
# unused tempf="$tempdir/tempf"

cleanup(){
    rm -r "$tempdir"
    trap - EXIT
}

msg(){
    echo "$@"
}

error(){
    echo "$@" >&2
}

fatal(){
    local rc=99

    case $# in
    1) ;;
    *) case "$1" in
       [0-9]|[0-9][0-9]|[0-9][0-9][0-9]) rc="$1"; shift ;;
       esac
    esac

    if [ $# -gt 0 ]; then
        error "$@"
    fi
    cleanup
    exit $rc
}

# This includes administrator-only programs
export PATH='/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'

# Globals
DUMMY_FILE='/apps/BMS-Firmware-imx7-M4/MDK/m4_tcm_dummy.bin'
FLAG_FILE='/m4_tcm.bin.EMERGENCY'
BIN_FILE='/m4_tcm.bin'

# Testing
# FLAG_FILE=E
# BIN_FILE=B

usage(){
    cat <<EOF
usage: $progname [ off | on ] | [ status ]

EOF
}

# Parse options
#

force=0 rc=0

# N.B. "::" would indicate optional arguments to an option flag
GETOPT=$(getopt -o h --long help \
     -n "$progname" -- "$@")
if [ $? != 0 ]; then
    usage
    exit 1
fi
eval set -- "$GETOPT"

while :; do
    case "$1" in
    -h|--help)               usage
                             exit 0
                             ;;
    --) shift; break ;;
    *) usage; exit 1 ;;
    esac
done

case "$1" in
status) action=status ;;
'')     action=status ;;
off)    action=off ;;
on)     action=on ;;
*)      usage
        exit 1 ;;
esac
     
# Did options parsing fail?
[ $rc -ne 0 ] && exit 1

# Main functions
#

status_is_on(){
    [ -f "$FLAG_FILE" ]
    return $?
}

turnoff(){
    if status_is_on; then
        :
    else
        fatal problem: there is no handbrake to turn off
    fi
    
    mv "$FLAG_FILE" "$BIN_FILE"
    return $?
}

countdown(){
    c="${1?}"
    while [ $c -ge 0 ]; do
        echo -e -n " \r$c"
        c="$(expr $c - 1 )"
        sleep 1
    done
    echo
}

turnon(){
    if status_is_on; then
        fatal problem: handbrake state is already on
    fi

    mv "$BIN_FILE" "$FLAG_FILE" && cp "$DUMMY_FILE" "$BIN_FILE"
    rc=$?
    # TESTING rc=0

    if [ $rc -ne 0 ]; then
        fatal problem: could not turn handbrake state on

    else
        # Give time for filesystem state to catch up before rapid reboot
        sync

        msg Rebooting in 5 seconds.
        countdown 5

        # TESTING echo BOOM; return

        systemctl --force --force reboot
        sleep 10
    fi
}    


# Main
#
rc=0

case "$action" in
status) if status_is_on; then
            echo on
        else
            echo off
        fi ;;

off)    turnoff
        rc=$? ;;

on)     turnon
        rc=$? ;;

*)      fatal "[action:$action]" 'This should never happen!'
esac

exit $rc
